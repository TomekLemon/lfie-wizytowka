<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="utf-8">
        <title>Lubelska Fundacja Inicjatyw Ekologicznych</title>
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,600,700" rel="stylesheet">
        <link rel="stylesheet" href="styles.css?ver=1.0.3"/>
        <meta name="viewport" content="width=device-width, inital-scale=1">
    </head>
    <body>
        <div class="whole-screen">
            <div class="container">
                <div class="header brand">
                    <img src="/assets/images/logo.png" alt="Lubelska Fundacja Inicjatyw Ekologicznych" />
                </div>
                <div class="content">
                    <div class="column">
                        <div class="short-line"></div>
                        <p>
                            <strong>Lubelska Fundacja Inicjatyw Ekologicznych</strong> specjalizuje się w działalności usługowej i doradczej w zakresie opracowywania dokumentów strategicznych, pozyskiwania dotacji z funduszy zewnętrznych, jak również w zakresie realizacji przedsięwzięć inwestycyjnych i szkoleniowych.<br />
Swoje wsparcie kierujemy do podmiotów sektora finansów publicznych, organizacji pozarządowych oraz biznesu.
                        </p>
                    </div>
                    <div class="column contact-info">
                        <div class="item phone-section">
                            <img src="/assets/images/phone-icon.png" alt="Telefon" />
                            <div class="diversed-item">
                                <p>
                                    608 778 898<br />
                                    608 461 834
                                </p>
                                <p>
                                    fax. 81 470 72 83
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/assets/images/mail-icon.png" alt="Telefon" />
                            <div class="diversed-item">
                                <p>
                                    biuro@lfie.pl
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/assets/images/www-icon.png" alt="Telefon" />
                            <div class="diversed-item">
                                <p>
                                    www.lfie.pl
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
